AWS CLI Builder
===============
Dockerfile with AWS CLI used for deployment for various builders.

Builders
--------
- base-alpine
- mssql-newman-amazonlinux
- amazon-linux:python3.6
- amazon-linux:newman
- node-alpine
- node-8.11.0-alpine
- node-firefox-alpine
- node-ionic-alpine
- node-10-ionic-alpine
- node-8.11.0-ionic-alpine
- python3.6-alpine
- python3.6-chalice-alpine
- python3.6-zappa-alpine / python-zappa-alpine
- python3.6-zappa-newman-alpine
- pyhton3.6-zappa-gcc-alpine
- python3.6-zappa-gcc-newman-alpine
- python3.7-alpine / python-alpine
- python3.7-chalice-alpine / python-chalice-alpine
- python3.6-zappa-alpine
- python3.7-zappa-newman-alpine
- pyhton3.7-zappa-gcc-alpine
- python3.7-zappa-gcc-newman-alpine
- cticti/pandas:python3.7-alpine

